/* 1. Які існують типи даних у Javascript?

JavaScript має 8 основних типів даних.

2. У чому різниця між == і ===?

Оператор суворої рівності ===, на відміну від ==, перевіряє рівність без приведення типів.

3. Що таке оператор?

JavaScript підтримує бінарні та унарні оператори, а також ще один спеціальний тернарний оператор - умовний оператор.

*/

let userName = '';

while (userName === '') {
  userName = String(prompt('Please enter your name', userName));
}

let userAge = '';

while (!/^[0-9]+$/.test(userAge)) {
  userAge = prompt('Please enter your age (0-9 allowed)', userAge);
}

if (userAge < 18) {
  alert('You are not allowed to visit this website');
} else {
  if (userAge > 22) {
    alert('Welcome, ' + userName);
  } else {
    if (userAge >= 18) {
      const check = confirm('Are you sure you want to continue?');
      if (check) {
        alert('Welcome, ' + userName);
      } else {
        alert('You are not allowed to visit this website');
      }
    }
  }
}
